package ir.coleo.alexa.chayi.callBack;

import java.util.ArrayList;

import ir.coleo.alexa.chayi.Chayi;

public interface ChayiCallBack extends CallBack {


    void onResponse(ArrayList<Chayi> chayis);


}
