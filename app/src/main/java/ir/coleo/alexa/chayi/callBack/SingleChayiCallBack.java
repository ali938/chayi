package ir.coleo.alexa.chayi.callBack;

import ir.coleo.alexa.chayi.Chayi;

public interface SingleChayiCallBack extends CallBack {

    void onResponse(Chayi chayi);


}
